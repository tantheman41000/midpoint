package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.Product
import dv.spring.kotlin.entity.dto.PageProductDto
import dv.spring.kotlin.entity.dto.PageSelectedProductDto
import dv.spring.kotlin.entity.dto.ProductDto
import dv.spring.kotlin.service.ProductService
import dv.spring.kotlin.service.SelectedProductService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.*

@RestController

class ProductController {

    @Autowired
    lateinit var productService: ProductService

    @Autowired
    lateinit var selectedProductService: SelectedProductService

    @GetMapping("/products")
    fun getProducts(): ResponseEntity<Any> {
        val products = productService.getProducts()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapProductDto(products))
    }

    @GetMapping("/product/query")
    fun getProducts(@RequestParam("name") name: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapProductDto(productService.getProductByName(name))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/products/partialQuery")
    fun getProductPartial(@RequestParam("name") name: String,
                          @RequestParam(value = "desc", required = false) desc: String?): ResponseEntity<Any> {

        val output: List<Any>
        if (desc == null) {
            output = MapperUtil.INSTANCE.mapProductDto(productService.getProductByPartialName(name))
        } else {
            output = MapperUtil.INSTANCE.mapProductDto(productService.getProductByPartialNameAndDesc(name, desc))
        }
        return ResponseEntity.ok(output)

//        var output = MapperUtil.INSTANCE.mapProductDto(productService.getProductByPartialName(name))
//        output?.let { return ResponseEntity.ok(it) }
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/products/{manuName}")
    fun getProductByManuName(@PathVariable("manuName") name: String): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapProductDto(
                productService.getProductByManuName(name))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/products/name")
    fun getProductWithPage(@RequestParam("name") name: String,
                           @RequestParam("page") page: Int,
                           @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any> {
        val output = productService.getProductWithPage(name, page, pageSize)
        return ResponseEntity.ok(PageProductDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                products = MapperUtil.INSTANCE.mapProductDto(output.content)))
    }

    @GetMapping("/products/selected")
    fun getSelectedProductByName(@RequestParam("name") name: String,
                                 @RequestParam("page", required = false) page: Int,
                                 @RequestParam("pageSize", required = false) pageSize: Int): ResponseEntity<Any> {

        if (page == null && pageSize == null) {
            val output = MapperUtil.INSTANCE.mapSelectedProduct(selectedProductService.getSelectedProductByName(name))
            return ResponseEntity.ok(output)
        } else {
            val output = selectedProductService.getSelectedProductWithPage(name, page, pageSize)
            return ResponseEntity.ok(PageSelectedProductDto(totalPages = output.totalPages,
                    totalElements = output.totalElements,
                    products = MapperUtil.INSTANCE.mapSelectedProduct(output.content)))
        }
    }

    @PostMapping("/product/manufacturer/{manuId}")
    fun addProduct(@RequestBody productDto: ProductDto,
                   @PathVariable manuId: Long): ResponseEntity<Any> {
        val output = productService.save(manuId, MapperUtil.INSTANCE.mapProductDto(productDto))
        val outputDto = MapperUtil.INSTANCE.mapProductDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @DeleteMapping("/product/{id}")
    fun deleteProduct(@PathVariable("id") id: Long): ResponseEntity<Any> {
        val product = productService.remove(id)
        val outputProduct = MapperUtil.INSTANCE.mapProductDto(product)
        outputProduct?.let {return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("The product id is not found!")
    }
}