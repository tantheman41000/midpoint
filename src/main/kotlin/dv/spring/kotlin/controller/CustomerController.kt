package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.ShoppingCart
import dv.spring.kotlin.entity.ShoppingCartStatus
import dv.spring.kotlin.entity.UserStatus
import dv.spring.kotlin.entity.dto.CustomerDto
import dv.spring.kotlin.service.CustomerService
import dv.spring.kotlin.service.ShoppingCartService
import dv.spring.kotlin.util.MapperUtil
import org.mapstruct.Mapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class CustomerController {
    @Autowired
    lateinit var customerService: CustomerService

    @Autowired
    lateinit var shoppingCartService: ShoppingCartService

    @GetMapping("/customers")
    fun getCustomers(): ResponseEntity<Any> {
        val customers = customerService.getCustomers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(customers))
    }

    @GetMapping("/customer/query")
    fun getProducts(@RequestParam("name") name: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByName(name))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/customers/partialQuery")
    fun getCustomerPartial(@RequestParam("name") name: String,
                           @RequestParam(value = "email", required = false) email: String?): ResponseEntity<Any> {

        val output: List<Any>
        if (email == null) {
            output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByPartialName(name))
        } else {
            output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByPartialNameAndEmail(name, email))
        }
        return ResponseEntity.ok(output)

//        var output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByPartialName(name))
//        output?.let { return ResponseEntity.ok(it) }
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/customers/byProvince")
    fun getCustomerByProvince(@RequestParam("province") province: String): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByProvince(province))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customers/findByStatus")
    fun getCustomersByStatus(@RequestParam("status") status: UserStatus): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomersByStatus(status))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customers/findByProductName")
    fun getCustomersByProductName(@RequestParam("name") name: String): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomersByProductName(name))
        return ResponseEntity.ok(output)
    }
//
    @PostMapping("/customer")
    fun addCustomer(@RequestBody customerDto: CustomerDto) : ResponseEntity<Any> {
        val output = customerService.save(MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PostMapping("/customer/defaultAddress/{addressId}")
    fun addCustomer(@RequestBody customerDto: CustomerDto,
                    @PathVariable addressId: Long): ResponseEntity<Any> {
        val output = customerService.save(addressId, MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @DeleteMapping("/customer/{id}")
    fun deleteCustomer(@PathVariable("id") id: Long): ResponseEntity<Any> {
        val customer = customerService.remove(id)
        val outputCustomer = MapperUtil.INSTANCE.mapCustomerDto(customer)
        outputCustomer?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("The customer id is not found")
    }

}