package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.Manufacturer
import dv.spring.kotlin.entity.dto.ManufacturerDto
import dv.spring.kotlin.service.ManufacturerService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ManufacturerController {

    @Autowired
    lateinit var manufacturerService: ManufacturerService

    @GetMapping("/manufacturers")
    fun getAllManufacturer(): ResponseEntity<Any> {
        val manufactures = manufacturerService.getManufacturers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapManufacturer(manufactures))
    }

    @PostMapping("/manufacturer")
    fun addManufacturer(@RequestBody manu: ManufacturerDto): ResponseEntity<Any> {
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapManufacturer(
                        manufacturerService.save(manu)))
    }

    @PutMapping("/manufacturer")
    fun updateManufacturer(@RequestBody manu: ManufacturerDto): ResponseEntity<Any> {
        if (manu.id == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Id must not be NULL")
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapManufacturer(
                manufacturerService.save(manu)
        ))
    }

    @PutMapping("/manufacturer/{manuId}")
    fun updateManufacturer(@PathVariable("manuId") id: Long?,
                           @RequestBody manu: ManufacturerDto): ResponseEntity<Any> {
        manu.id = id
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapManufacturer(
                manufacturerService.save(manu)
        ))
    }
}