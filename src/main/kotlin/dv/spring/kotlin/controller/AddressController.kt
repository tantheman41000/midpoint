package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.dto.AddressDto
import dv.spring.kotlin.service.AddressService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class AddressController {
    @Autowired
    lateinit var addressService: AddressService

    @GetMapping("/addresses")
    fun getAddresses(): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapAddressDto(addressService.getAddresses())
        return ResponseEntity.ok(output)
    }

    @PostMapping("/address")
    fun addAddress(@RequestBody address: AddressDto): ResponseEntity<Any> {
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapAddressDto(
                        addressService.save(address)
                )
        )
    }

    @PutMapping("/address")
    fun updateManufacturer(@RequestBody address: AddressDto): ResponseEntity<Any> {
        if (address.id == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Id must not be NULL")
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapAddressDto(
                addressService.save(address)
        ))
    }

    @PutMapping("/address/{addressId}")
    fun updateManufacturer(@PathVariable("addressId") id: Long?,
                           @RequestBody address: AddressDto): ResponseEntity<Any> {
        address.id = id
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapAddressDto(
                addressService.save(address)
        ))
    }
}