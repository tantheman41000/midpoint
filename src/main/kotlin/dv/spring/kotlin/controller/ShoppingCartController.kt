package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.dto.PageShoppingCartDto
import dv.spring.kotlin.service.ShoppingCartService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class ShoppingCartController {
    @Autowired
    lateinit var shoppingCartService: ShoppingCartService

    @GetMapping("/shoppingCarts")
    fun getShoppingCarts(@RequestParam("page", required = false) page: Int,
                         @RequestParam("pageSize", required = false) pageSize: Int): ResponseEntity<Any> {
        if (page != null && pageSize != null) {
            val output = shoppingCartService.getShoppingCartsWithPage(page, pageSize)
            return ResponseEntity.ok(PageShoppingCartDto(
                    totalPages = output.totalPages,
                    totalElements = output.totalElements,
                    shoppingCarts = MapperUtil.INSTANCE.mapShoppingCarts(output.content))
            )
        } else {
            val shoppingCarts = shoppingCartService.getShoppingCarts()
            return ResponseEntity.ok(MapperUtil.INSTANCE.mapShoppingCarts(shoppingCarts))
        }
    }

    @GetMapping("/shoppingCarts/findByProductName")
    fun getShoppingCarts(@RequestParam("name") name: String,
                         @RequestParam("page", required = false) page: Int,
                         @RequestParam("pageSize", required = false) pageSize: Int): ResponseEntity<Any> {
        if (page != null && pageSize != null) {
            val output = shoppingCartService.getShoppingCartsByNameWithPage(name,page, pageSize)
            return ResponseEntity.ok(PageShoppingCartDto(
                    totalPages = output.totalPages,
                    totalElements = output.totalElements,
                    shoppingCarts = MapperUtil.INSTANCE.mapShoppingCarts(output.content))
            )
        } else {
            val output = shoppingCartService.getShoppingCartsByProductName(name)
            return ResponseEntity.ok(MapperUtil.INSTANCE.mapShoppingCarts(output))
        }
    }

}