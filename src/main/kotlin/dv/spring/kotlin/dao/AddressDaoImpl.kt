package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Address
import dv.spring.kotlin.repository.AddressRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class AddressDaoImpl : AddressDao {
    @Autowired
    lateinit var addressRepository: AddressRepository

    override fun getAddresses(): List<Address> {
        return addressRepository.findAll().filterIsInstance(Address::class.java)
    }

    override fun save(address: Address): Address {
        return addressRepository.save(address)
    }

    override fun findById(addressId: Long): Address? {
        return addressRepository.findById(addressId).orElse(null)
    }
}