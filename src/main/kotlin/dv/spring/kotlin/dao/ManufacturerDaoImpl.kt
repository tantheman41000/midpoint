//package dv.spring.kotlin.dao
//
//import dv.spring.kotlin.entity.Manufacturer
//import org.springframework.context.annotation.Profile
//import org.springframework.stereotype.Repository
//
//@Profile("mem")
//@Repository
//class ManufacturerDaoImpl: ManufacturerDao {
//    override fun getManufacturers(): List<Manufacturer> {
//        return mutableListOf(Manufacturer("Apple", "053123456"),
//                Manufacturer("Samsung", "5556666777888"),
//                Manufacturer("CAMT", "00000000"))
//    }
//}