package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.SelectedProduct
import dv.spring.kotlin.repository.SelectedProductRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class SelectedProductDaoImpl: SelectedProductDao {
    @Autowired
    lateinit var selectedProductRepository: SelectedProductRepository

    override fun getSelectedProducts(): List<SelectedProduct> {
        return selectedProductRepository.findAll().filterIsInstance(SelectedProduct::class.java)
    }

    override fun getSelectedProducts(name: String): List<SelectedProduct> {
        return selectedProductRepository.findBySelectedProduct_NameContainingIgnoreCase(name)
    }

    override fun getSelectedProductWithPage(name: String, page: Int, pageSize: Int): Page<SelectedProduct> {
        return selectedProductRepository.findBySelectedProduct_NameContainingIgnoreCase(name, PageRequest.of(page,pageSize))
    }
}