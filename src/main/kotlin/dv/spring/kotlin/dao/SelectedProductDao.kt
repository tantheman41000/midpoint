package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.SelectedProduct
import org.springframework.data.domain.Page

interface SelectedProductDao {
    fun getSelectedProducts(): List<SelectedProduct>
    fun getSelectedProducts(name: String): List<SelectedProduct>
    fun getSelectedProductWithPage(name: String, page: Int, pageSize: Int): Page<SelectedProduct>
}