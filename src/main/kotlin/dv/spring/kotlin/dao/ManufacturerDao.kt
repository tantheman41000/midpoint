package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Manufacturer
import dv.spring.kotlin.entity.dto.ManufacturerDto

interface ManufacturerDao {
    fun getManufacturers(): List<Manufacturer>
    fun save(manufacturer: Manufacturer): Manufacturer
    fun findById(manuId: Long): Manufacturer?
}