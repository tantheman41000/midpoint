package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.ShoppingCart
import dv.spring.kotlin.entity.ShoppingCartStatus
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface ShoppingCartDao {
    fun getShoppingCarts(): List<ShoppingCart>
    fun getShoppingCartsWithPage(page: Int, pageSize: Int): Page<ShoppingCart>
    fun getShoppingCartsByProductName(name: String): List<ShoppingCart>
    fun getShoppingCartsByNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun getShoppingCartsByProductNameAndStatus(name: String, paid: ShoppingCartStatus): List<ShoppingCart>
}