package dv.spring.kotlin.util

import dv.spring.kotlin.entity.*
import dv.spring.kotlin.entity.dto.*
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    @Mappings(Mapping(source = "manufacturer", target = "manu"))
    fun mapProductDto(product: Product?): ProductDto?
    fun mapProductDto(products: List<Product>): List<ProductDto>
    @InheritInverseConfiguration
    fun mapProductDto(product: ProductDto): Product

    fun mapManufacturer(manu: Manufacturer): ManufacturerDto
    fun mapManufacturer(manu: List<Manufacturer>): List<ManufacturerDto>
    @InheritInverseConfiguration
    fun mapManufacturer(manu: ManufacturerDto): Manufacturer

    @Mappings(Mapping(source = "defaultAddress", target = "defAddress"))
    fun mapCustomerDto(customer: Customer?): CustomerDto?
    fun mapCustomerDto(customers: List<Customer>): List<CustomerDto>
    @InheritInverseConfiguration
    fun mapCustomerDto(customer: CustomerDto): Customer

    fun mapAddressDto(address: Address): AddressDto
    fun mapAddressDto(address: List<Address>): List<AddressDto>
    @InheritInverseConfiguration
    fun mapAddressDto(address: AddressDto): Address

    fun mapShoppingCarts(shoppingCart: ShoppingCart?): ShoppingCartDto
    fun mapShoppingCarts(shoppingCarts: List<ShoppingCart>): List<ShoppingCartDto>

    @Mappings(Mapping(source = "selectedProduct", target = "selected"))
    fun mapSelectedProduct(selectedProduct: SelectedProduct?): SelectedProductDto?
    fun mapSelectedProduct(selectedProducts: List<SelectedProduct>): List<SelectedProductDto>








}