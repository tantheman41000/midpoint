package dv.spring.kotlin.service

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.ShoppingCart
import dv.spring.kotlin.entity.ShoppingCartStatus
import org.springframework.data.domain.Page

interface ShoppingCartService {
    fun getShoppingCarts(): List<ShoppingCart>
    fun getShoppingCartsWithPage(page: Int, pageSize: Int): Page<ShoppingCart>
    fun getShoppingCartsByProductName(name: String): List<ShoppingCart>
    fun getShoppingCartsByNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
}