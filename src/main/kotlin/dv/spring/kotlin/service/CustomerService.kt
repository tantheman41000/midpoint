package dv.spring.kotlin.service

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.UserStatus

interface CustomerService {
    fun getCustomers(): List<Customer>
    fun getCustomerByName(name:String): Customer?
    fun getCustomerByPartialName(name:String): List<Customer>
    fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer>
    fun getCustomerByProvince(province: String): List<Customer>
    fun getCustomersByStatus(status: UserStatus): List<Customer>
    fun getCustomersByProductName(name: String): List<Customer>
    fun save(customer: Customer): Customer
    fun save(addressId: Long, customer: Customer): Customer
    fun remove(id: Long): Customer?
}