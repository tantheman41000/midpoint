package dv.spring.kotlin.service

import dv.spring.kotlin.dao.ShoppingCartDao
import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.ShoppingCart
import dv.spring.kotlin.entity.ShoppingCartStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class ShoppingCartServiceImpl : ShoppingCartService {
    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao

    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingCartDao.getShoppingCarts()
    }

    override fun getShoppingCartsWithPage(page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.getShoppingCartsWithPage(page, pageSize)
    }

    override fun getShoppingCartsByProductName(name: String): List<ShoppingCart> {
        return shoppingCartDao.getShoppingCartsByProductName(name)
    }

    override fun getShoppingCartsByNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.getShoppingCartsByNameWithPage(name, page, pageSize)
    }


}