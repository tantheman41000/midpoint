package dv.spring.kotlin.service

import dv.spring.kotlin.entity.SelectedProduct
import org.springframework.data.domain.Page

interface SelectedProductService {
    fun getSelectedProducts(): List<SelectedProduct>
    fun getSelectedProductByName(name: String): List<SelectedProduct>
    fun getSelectedProductWithPage(name: String, page: Int, pageSize: Int): Page<SelectedProduct>
}