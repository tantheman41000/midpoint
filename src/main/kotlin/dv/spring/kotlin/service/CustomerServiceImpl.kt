package dv.spring.kotlin.service

import dv.spring.kotlin.dao.AddressDao
import dv.spring.kotlin.dao.CustomerDao
import dv.spring.kotlin.dao.ShoppingCartDao
import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.ShoppingCartStatus
import dv.spring.kotlin.entity.UserStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class CustomerServiceImpl : CustomerService {
    @Autowired
    lateinit var customerDao: CustomerDao

    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao

    @Autowired
    lateinit var addressDao: AddressDao

    override fun getCustomers(): List<Customer> {
        return customerDao.getCustomers()
    }

    override fun getCustomerByName(name: String): Customer? {
        return customerDao.getCustomerByName(name)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerDao.getCustomerByPartialName(name)
    }

    override fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer> {
        return customerDao.getCustomerByPartialNameAndEmail(name, email)
    }

    override fun getCustomerByProvince(province: String): List<Customer> {
        return customerDao.getCustomerByProvince(province)
    }

    override fun getCustomersByStatus(status: UserStatus): List<Customer> {
        return customerDao.getCustomersByStatus(status)
    }

    override fun getCustomersByProductName(name: String): List<Customer> {
//        val shoppingCarts = shoppingCartDao.getShoppingCartsByProductNameAndStatus(name, ShoppingCartStatus.PAID)
        val shoppingCarts = shoppingCartDao.getShoppingCartsByProductName(name)
        var customers = mutableListOf<Customer>()
        for (cart in shoppingCarts) {
            if (!customers.contains(cart.customer))
                customers.add(cart.customer!!)
        }
        return customers
    }

    @Transactional
    override fun save(customerIn: Customer): Customer {
        var customer = customerIn
        val address = addressDao.save(customerIn.defaultAddress!!)
        customer.defaultAddress = null
        val customerOutput = customerDao.save(customer)
        customerOutput.defaultAddress = address
        return customerOutput
    }

    @Transactional
    override fun save(addressId: Long, customerIn: Customer): Customer {
        val address = addressDao.findById(addressId)
        val customer = customerDao.save(customerIn)
        customer.defaultAddress = address
        return customer
    }

    @Transactional
    override fun remove(id: Long): Customer? {
        val customer = customerDao.findById(id)
        customer?.isDeleted = true
        return customer
    }
}