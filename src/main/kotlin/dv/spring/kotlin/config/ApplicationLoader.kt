package dv.spring.kotlin.config

import dv.spring.kotlin.entity.*
import dv.spring.kotlin.entity.SelectedProduct
import dv.spring.kotlin.repository.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
class ApplicationLoader : ApplicationRunner {
    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository
    @Autowired
    lateinit var productRepository: ProductRepository
    @Autowired
    lateinit var customerRepository: CustomerRepository
    @Autowired
    lateinit var addressRepository: AddressRepository
    @Autowired
    lateinit var selectedProductRepository: SelectedProductRepository
    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository
    @Autowired
    lateinit var dataLoader: DataLoader

    @Transactional
    override fun run(args: ApplicationArguments?) {

        // All Manufactures
        val camt = manufacturerRepository.save(Manufacturer("CAMT", "0000000"))
        val apple = manufacturerRepository.save(Manufacturer("Apple", "053123456"))
        val samsung = manufacturerRepository.save(Manufacturer("SAMSUNG", "5556666777888"))

        // All Products
        val productCamt = productRepository.save(Product("CAMT",
                "The best College in CMU",
                0.0,
                1,
                "http://www.camt.cmu.ac.th/th/images/logo.jpg"))
        camt.products.add(productCamt)
        productCamt.manufacturer = camt

        val productIPhone = productRepository.save(Product("iPhone",
                "It's a phone",
                28000.00,
                20,
                apple,
                "https://www.jaymartstore.com/Products/iPhone-X-64GB-Space-Grey--1140900010552--4724"))
        apple.products.add(productIPhone)
        productIPhone.manufacturer = apple

        val productNote9 = productRepository.save(Product("Note 9",
                "Other Iphone",
                28001.00,
                10,
                samsung,
                "http://dynamic-cdn.eggdigital.com/e56zBiUt1.jpg"))
        samsung.products.add(productNote9)
        productNote9.manufacturer = samsung

        val productPrayuth = productRepository.save(Product("Prayuth",
                "The best PM ever",
                1.00,
                1,
                camt,
                "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Prayut_Chan-o-cha_%28cropped%29_2016.jpg/200px-Prayut_Chan-o-cha_%28cropped%29_2016.jpg"))
        camt.products.add(productPrayuth)
        productPrayuth.manufacturer = camt

        // All Customers
        val customer1 = customerRepository.save(Customer("Lung",
                "pm@go.th",
                UserStatus.ACTIVE))
        customer1.defaultAddress = addressRepository.save(Address("ถนนอนุสาวรีย์ประชาธิปไตย",
                "แขวง ดินสอ",
                "เขตดุสิต",
                "กรุงเทพ","10123"))

        val customer2 = customerRepository.save(Customer("ชัชชาติ",
                "chut@taopoon.com",
                UserStatus.ACTIVE))
        customer2.defaultAddress = addressRepository.save(Address("239 มหาวิทยาลัยเชียงใหม่",
                "ต.สุเทพ",
                "อ.เมือง",
                "จ.เชียงใหม่","50200"))

        val customer3 = customerRepository.save(Customer("ธนาธร",
                "thanathorn@life.com"))
        customer3.defaultAddress = addressRepository.save(Address("ซักที่บนโลก",
                "ต.สุขสันต์",
                "อ.ในเมือง",
                "จ.ขอนแก่น่","12457"))

        // All Selected Products
        val selected1 = selectedProductRepository.save(SelectedProduct(4))
        selected1.selectedProduct = productIPhone

        val selected2 = selectedProductRepository.save(SelectedProduct(1))
        selected2.selectedProduct = productPrayuth

        val selected3 = selectedProductRepository.save(SelectedProduct(1))
        selected3.selectedProduct = productPrayuth

        val selected4 = selectedProductRepository.save(SelectedProduct(1))
        selected4.selectedProduct = productCamt

        val selected5 = selectedProductRepository.save(SelectedProduct(2))
        selected5.selectedProduct = productNote9

        // All Shopping Carts
        val cart1 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.SENT))
        cart1.customer = customer1
        cart1.selectedProducts.add(selected1)
        cart1.selectedProducts.add(selected2)
        cart1.shippingAddress = customer1.defaultAddress!!

        val cart2 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.WAIT))
        cart2.customer = customer2
        cart2.selectedProducts.add(selected3)
        cart2.selectedProducts.add(selected4)
        cart2.selectedProducts.add(selected5)
        cart2.shippingAddress = customer2.defaultAddress!!

        dataLoader.loadData()

    }
}