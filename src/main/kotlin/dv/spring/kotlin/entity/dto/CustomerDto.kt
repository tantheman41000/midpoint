package dv.spring.kotlin.entity.dto

import dv.spring.kotlin.entity.Address
import dv.spring.kotlin.entity.UserStatus

class CustomerDto(var id: Long? = null,
                  var name: String? = null,
                  var email: String? = null,
                  var userStatus: UserStatus? = UserStatus.PENDING,
                  var defAddress: Address? = null) {
}