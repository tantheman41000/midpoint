package dv.spring.kotlin.entity

import javax.persistence.*
import kotlin.jvm.Transient

@Entity
data class ShoppingCart(var status: ShoppingCartStatus? = ShoppingCartStatus.WAIT) {

    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToMany
    var selectedProducts = mutableListOf<SelectedProduct>()

    @ManyToOne
    var customer: Customer? = null

    @ManyToOne
    var shippingAddress: Address? = null
}