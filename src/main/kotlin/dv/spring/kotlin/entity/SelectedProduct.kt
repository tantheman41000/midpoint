package dv.spring.kotlin.entity

import javax.persistence.*
import kotlin.jvm.Transient

@Entity
data class SelectedProduct(var quantity: Int? = null) {

    @Id
    @GeneratedValue
    var id:Long? = null

    @OneToOne
    var selectedProduct: Product? = null
}