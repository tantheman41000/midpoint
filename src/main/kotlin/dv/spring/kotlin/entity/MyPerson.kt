package dv.spring.kotlin.entity

data class MyPerson(var name:String, var surname:String, var team:String, var number:Int)