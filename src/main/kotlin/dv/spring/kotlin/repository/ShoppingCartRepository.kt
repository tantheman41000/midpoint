package dv.spring.kotlin.repository

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.ShoppingCart
import dv.spring.kotlin.entity.ShoppingCartStatus
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository

interface ShoppingCartRepository : CrudRepository<ShoppingCart, Long> {
    fun findAll(page: Pageable): Page<ShoppingCart>
    fun findBySelectedProducts_SelectedProduct_NameContainingIgnoreCase(name: String): List<ShoppingCart>
    fun findBySelectedProducts_SelectedProduct_NameContainingIgnoreCase(name: String, page: Pageable): Page<ShoppingCart>
    fun findBySelectedProducts_SelectedProduct_NameContainingIgnoreCaseAndStatus(name: String, paid: ShoppingCartStatus): List<ShoppingCart>
}